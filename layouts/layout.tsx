import React from "react";
import styled from "styled-components";

const StyledLayout = styled.div`
    height: 100%;
`;

export default function Layout({ children }) {
    return <StyledLayout>{children}</StyledLayout>
}