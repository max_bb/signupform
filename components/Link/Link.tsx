import React from "react";
import styled from "styled-components";
import NextJsLink from 'next/link'

import {ILink} from "./types";
import {actionColor} from "../../generic/styleConstants";

const StyledLink = styled.div`
    display: inline-block;
    color: ${actionColor};
    text-decoration: none;
`

const Link: React.FC<ILink> = (
    {
        href,
        isExternal = false,
        children,
    }
) => {
    return (
        <StyledLink>
            {isExternal ?
                <a href={href} target="_blank">{children}</a> :
                <NextJsLink href={href}>{children}</NextJsLink>}
        </StyledLink>
    )
}

export default Link;