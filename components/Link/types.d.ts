import {ReactNode} from "react";

export interface ILink {
    href: string,
    isExternal?: boolean,
    children: ReactNode,
}