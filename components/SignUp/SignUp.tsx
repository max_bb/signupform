import React from "react";
import styled from "styled-components";
import {useFormik} from "formik";
import * as yup from 'yup';

import {
    TextInput,
    EmailInput,
    PasswordInput
} from "../../generic/components/Input";
import {genderList} from "../../generic/constants";
import {RadioGroup} from "../../generic/components/Radio";
import Checkbox from "../../generic/components/Checkbox";
import Link from "../../components/Link";
import {AsyncActionButton} from "../../generic/components/Button";
import {errorColor, textColor} from "../../generic/styleConstants";

const StyledForm = styled.form`
    width: 400px;
    background: #fff;
    border-radius: 8px;
    padding: 32px 28px 53px;
`;

const StyledFormTitle = styled.div`
    font-weight: bold;
    color: ${textColor};
    font-size: 28px;
    text-align: center;
    margin-bottom: 37px;
`;

const StyledFormField = styled.div`
    padding-bottom: 21px;
    position: relative;
`;

const StyledFormInvisibleDivider = styled.div<{ size?: 'large' | 'normal' }>`
    height: ${props => props.size === 'large' ? '16px' : '9px'};
`;

const StyledFormFieldError = styled.div`
    position: absolute;
    bottom: 6px;
    left: 17px;
    color: ${errorColor};
    font-size: 10px;
`;

const SignUp = () => {
    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            gender: '',
            acceptPolicies: false,
        },
        onSubmit: async values => {
            console.log(values);
            await new Promise(resolve => setTimeout(resolve, 1000));
        },
        validateOnChange: false,
        validationSchema: yup.object().shape({
            name: yup.string()
                .matches(/^[A-Za-z]*$/, 'Please enter a valid name')
                .required('You must specify your name'),
            email: yup.string()
                .email('Please enter a valid email address')
                .required('You must specify your email'),
            password: yup.string()
                .min(6, 'Password must contain at least 6 symbols')
                .required('You must specify your password'),
            gender: yup.string()
                .required('You must select the gender'),
            acceptPolicies: yup.boolean().oneOf([true], 'You must accept the policies'),
        })
    });

    const isAllFieldsFilled = Object.values(formik.values).every(fieldValue => !!fieldValue);

    return (
        <StyledForm>
            <StyledFormTitle>Create a new account</StyledFormTitle>
            <StyledFormField>
                <TextInput
                    placeholder="Enter your name"
                    value={formik.values.name}
                    onChange={(value) => formik.setFieldValue('name', value)}
                />

                {formik.errors.name && <StyledFormFieldError>{formik.errors.name}</StyledFormFieldError>}
            </StyledFormField>

            <StyledFormField>
                <EmailInput
                    placeholder="Email"

                    value={formik.values.email}
                    onChange={(value) => formik.setFieldValue('email', value)}
                />

                {formik.errors.email && <StyledFormFieldError>{formik.errors.email}</StyledFormFieldError>}
            </StyledFormField>

            <StyledFormField>
                <PasswordInput
                    placeholder="Password"
                    value={formik.values.password}
                    onChange={(value) => formik.setFieldValue('password', value)}
                />

                {formik.errors.password &&
                <StyledFormFieldError>{formik.errors.password}</StyledFormFieldError>}
            </StyledFormField>

            <StyledFormInvisibleDivider/>

            <StyledFormField>
                <RadioGroup
                    buttonsList={genderList}
                    groupName="genderSelect"
                    value={formik.values.gender}
                    onChange={(value) => formik.setFieldValue('gender', value)}
                />

                {formik.errors.gender && <StyledFormFieldError>{formik.errors.gender}</StyledFormFieldError>}
            </StyledFormField>

            <StyledFormField>
                <Checkbox
                    label={
                        <>Accept <Link href="http://google.com" isExternal={true}>terms</Link> and <Link
                            href="http://google.com" isExternal={true}>conditions</Link></>
                    }
                    value={formik.values.acceptPolicies}
                    onChange={(value) => formik.setFieldValue('acceptPolicies', value)}
                />

                {formik.errors.acceptPolicies &&
                <StyledFormFieldError>{formik.errors.acceptPolicies}</StyledFormFieldError>}
            </StyledFormField>

            <StyledFormInvisibleDivider size="large"/>

            <AsyncActionButton
                type="submit"
                label="Sign up"
                onClick={formik.handleSubmit}
                isActionInProgress={formik.isSubmitting}
                isDisabled={!isAllFieldsFilled}
                isFullWidth={true}
            />
        </StyledForm>
    );
};

export default SignUp;