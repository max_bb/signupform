import React from "react";
import styled from "styled-components";

import {IRadioButton} from "./types";
import {actionColor, focusIndication, stylesToHideNativeInput} from "../../styleConstants";
import useUniqueId from "../useUniqueId";
import useFocus from "../useFocus";

const StyledNativeRadio = styled.input`
    ${stylesToHideNativeInput}
`;

const StyledRadioButtonWrapper = styled.div`
    display: flex;
    align-items: center;
    margin-right: 17px;
`;

const StyledRadioButton = styled.div<{isFocused: boolean}>`
    width: 14px;
    height: 14px;
    border: 1px solid ${actionColor};
    border-radius: 100%;
    position: relative;
    
    ${props => props.isFocused && focusIndication}
`;

const StyledRadioButtonBullet = styled.div<{isVisible: boolean}>`
    width: 100%;
    height: 100%;
    border: 3px solid #fff;
    border-radius: 100%;
    background-color: ${actionColor};
    
    opacity: ${props => props.isVisible ? 1 : 0}
`;

const StyledRadioButtonLabel = styled.label`
    font-size: 14px;
    padding-left: 8px;
`;

const RadioButton: React.FC<IRadioButton> = (
    {
        name,
        identifier,
        value,
        isChecked,
        onChange,
    }
) => {
    const {isFocused, handleBlur, handleFocus} = useFocus();
    const radioId = useUniqueId('radio-');

    const handleNativeRadioChange = () => {
        onChange(identifier);
    };

    return (
        <StyledRadioButtonWrapper>
            <StyledRadioButton isFocused={isFocused}>
                <StyledNativeRadio
                    type="radio"
                    id={radioId}
                    name={name}
                    value={value}
                    onChange={handleNativeRadioChange}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                />
                <StyledRadioButtonBullet data-test-id="bullet" isVisible={isChecked} />
            </StyledRadioButton>
            <StyledRadioButtonLabel htmlFor={radioId} data-test-id="label">{value}</StyledRadioButtonLabel>
        </StyledRadioButtonWrapper>
    )
}

export default RadioButton;