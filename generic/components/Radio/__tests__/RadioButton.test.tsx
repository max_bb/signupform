import React from "react";
import { shallow } from 'enzyme';

import RadioButton from "../RadioButton";

const testProps = {
    identifier: 'mockIdentifier',
    value: 'mockValue',
    isChecked: true,
    onClick: () => void(0),
}

const commonWrapper = shallow(<RadioButton {...testProps} />);

describe('RadioButton component', () => {
    it('Should render correct label', function () {
        expect(commonWrapper.find('[data-test-id="label"]').text()).toEqual(testProps.value);
    });

    it('Should be in correct state dependant on value', function () {
        expect(commonWrapper.find('[data-test-id="bullet"]')).toHaveLength(1);
    });

    it('Should call onChange handler on click', function () {
        const onChange = jest.fn();
        const wrapper = shallow(<RadioButton {...testProps} onClick={onChange} />);
        wrapper.simulate('click');
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange.mock.calls[0][0]).toEqual(testProps.identifier);
    });
})