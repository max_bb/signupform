import {IListValue} from '../../types';

export interface IRadioButton extends IListValue {
    name: string,
    isChecked: boolean,
    onChange: (key: string) => void,
}

export interface IRadioGroup {
    buttonsList: IListValue[],
    groupName: string,
    value: string,
    onChange: (value: string) => void,
}