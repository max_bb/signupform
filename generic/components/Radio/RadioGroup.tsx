import React, {useState} from "react";
import styled from "styled-components";

import {IRadioGroup} from "./types";
import RadioButton from "./RadioButton";

const StyledRadioGroup = styled.div`
    display: flex;
`;

const RadioGroup: React.FC<IRadioGroup> = (
    {
        buttonsList,
        groupName,
        value,
        onChange,
    }
) => {
    const [checkedIdentifier, setCheckedIdentifier] = useState(value || '');

    const handleRadioChange = (identifier: string) => {
        setCheckedIdentifier(identifier);
        onChange(identifier);
    };

    return (
        <StyledRadioGroup>
            {buttonsList.map(({identifier, value}) =>
                <RadioButton
                    name={groupName}
                    key={identifier}
                    identifier={identifier}
                    value={value}
                    isChecked={checkedIdentifier === identifier}
                    onChange={handleRadioChange}
                />
            )}
        </StyledRadioGroup>
    )
}

export default RadioGroup;