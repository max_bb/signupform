import {ReactNode} from "react";

export interface ICheckbox {
    label: string | ReactNode,
    value: boolean,
    onChange: (value: boolean) => void
}