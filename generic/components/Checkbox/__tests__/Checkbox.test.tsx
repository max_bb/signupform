import React from "react";
import { shallow } from 'enzyme';

import Checkbox from "../Checkbox";

const testProps = {
    label: 'mockLabel',
    value: true,
    onChange: () => void(0),
}

const commonWrapper = shallow(<Checkbox {...testProps} />);

describe('Checkbox component', () => {
    it('Should render correct label', function () {
        expect(commonWrapper.find('[data-test-id="label"]').text()).toEqual(testProps.label);
    });

    it('Should be in correct state dependant on value', function () {
        expect(commonWrapper.find('[data-test-id="checkedIcon"]')).toHaveLength(1);
    });

    it('Should call onChange handler on click', function () {
        const onChange = jest.fn();
        const wrapper = shallow(<Checkbox {...testProps} onChange={onChange} />);
        wrapper.simulate('click');
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange.mock.calls[0][0]).toEqual(false);
    });
})