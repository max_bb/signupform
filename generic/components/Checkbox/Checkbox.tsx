import React from "react";
import styled from "styled-components";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheck} from '@fortawesome/free-solid-svg-icons';

import {ICheckbox} from "./types";
import {actionColor, focusIndication, stylesToHideNativeInput} from "../../styleConstants";
import useFocus from "../useFocus";
import useUniqueId from "../useUniqueId";

const checkboxSize = 14;

const StyledNativeCheckbox = styled.input`
    ${stylesToHideNativeInput}
`;

const StyledCheckboxWrapper = styled.div`
    display: inline-flex;
    align-items: center;
`;

const StyledCheckbox = styled.div<{isFocused: boolean}>`
    width: ${checkboxSize}px;
    height: ${checkboxSize}px;
    border: 1px solid ${actionColor};
    text-align: center;
    position: relative;
    
    ${props => props.isFocused && focusIndication}
`;

const StyledCheckboxCheckedIcon = styled.div<{isVisible: boolean}>`
    color: ${actionColor};
    font-size: ${checkboxSize - 2}px;
    line-height: ${checkboxSize - 2}px;
    
    opacity: ${props => props.isVisible ? 1 : 0}
`;

const StyledCheckboxLabel = styled.label`
    font-size: ${checkboxSize}px;
    line-height: ${checkboxSize}px;
    padding-left: 8px;
`;

const Checkbox: React.FC<ICheckbox> = (
    {
        label,
        value,
        onChange,
    }
) => {
    const {isFocused, handleBlur, handleFocus} = useFocus();
    const checkboxId = useUniqueId('checkbox-');

    const handleNativeCheckboxChange = () => {
        onChange(!value)
    }

    return (
        <StyledCheckboxWrapper>
            <StyledCheckbox isFocused={isFocused}>
                <StyledNativeCheckbox
                    type="checkbox"
                    id={checkboxId}
                    value={value}
                    onChange={handleNativeCheckboxChange}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                />
                <StyledCheckboxCheckedIcon isVisible={value} data-test-id="checkedIcon">
                    <FontAwesomeIcon icon={faCheck}/>
                </StyledCheckboxCheckedIcon>
            </StyledCheckbox>
            <StyledCheckboxLabel htmlFor={checkboxId} data-test-id="label">{label}</StyledCheckboxLabel>
        </StyledCheckboxWrapper>
    )
}

export default Checkbox;