import {useEffect, useState} from "react";
import _uniqueId from "lodash/uniqueId";

const useUniqueId = (prefix) => {
    const [uniqueId, setUniqueId] = useState('');

    useEffect(() => {
        setUniqueId(_uniqueId(prefix));
    }, []);

    return uniqueId;
};

export default useUniqueId;