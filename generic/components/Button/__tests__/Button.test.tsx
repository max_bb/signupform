import React from "react";
import { shallow } from 'enzyme';

import Button from "../Button";

const testProps = {
    label: 'mockLabel',
    onClick: () => void(0),
}

describe('Button component', () => {
    it('Should render correct label', function () {
        const wrapper = shallow(<Button {...testProps} />);
        expect(wrapper.text()).toEqual(testProps.label);
    });

    it('Should call onClick handler on click', function () {
        const onClick = jest.fn();
        const wrapper = shallow(<Button {...testProps} onClick={onClick} />);
        wrapper.simulate('click');
        expect(onClick).toHaveBeenCalledTimes(1);
    });

    it('Should not call onClick when disabled', function () {
        const onClick = jest.fn();
        const wrapper = shallow(<Button {...testProps} onClick={onClick} isDisabled={true} />);
        wrapper.simulate('click');
        expect(onClick).toHaveBeenCalledTimes(0);
    });
})