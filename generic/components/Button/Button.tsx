import React from "react";
import styled from "styled-components";
import {IButton} from "./types";
import {actionColor, disabledColor, focusIndication, stylesToHideNativeInput} from "../../styleConstants";
import useFocus from "../useFocus";

const StyledButton = styled.div<{ isDisabled: boolean, isFullWidth: boolean, isFocused: boolean }>`
    padding: 20px 60px;
    display: inline-block;
    text-align: center;
    font-size: 18px;
    border-radius: 31px;
    color: #fff;
    position: relative;
    
    background-color: ${props => props.isDisabled ? disabledColor : actionColor};
    ${props => !props.isDisabled && 'cursor: pointer'};
    ${props => props.isFullWidth && 'width: 100%;'};
    
    ${props => props.isFocused && focusIndication};
`;

const StyledNativeButton = styled.button`
    ${stylesToHideNativeInput}
`

const Button: React.FC<IButton> = (
    {
        type = 'button',
        label,
        isDisabled,
        onClick,
        isFullWidth,
    }
) => {
    const {isFocused, handleBlur, handleFocus} = useFocus();

    return (
        <StyledButton
            isDisabled={isDisabled}
            isFullWidth={isFullWidth}
            isFocused={isFocused}
        >
            <StyledNativeButton
                type={type}
                onClick={onClick}
                disabled={isDisabled}
                onFocus={handleFocus}
                onBlur={handleBlur}
            >
                {label}
            </StyledNativeButton>
            {label}
        </StyledButton>
    )
}

export default Button;