import {ReactNode} from "react";

export interface IButton {
    type?: 'button' | 'submit',
    label: string | ReactNode,
    isDisabled?: boolean;
    onClick: () => void,
    isFullWidth?: boolean,
}

export interface IAsyncActionButton extends IButton{
    isActionInProgress?: boolean,
}