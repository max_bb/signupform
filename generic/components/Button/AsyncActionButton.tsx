import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

import {IAsyncActionButton} from "./types";
import Button from "./index";

const AsyncActionButton: React.FC<IAsyncActionButton> = (
    {
        isActionInProgress,
        label,
        ...rest
    }
) => {
    return (
        <Button
            label={isActionInProgress ? <FontAwesomeIcon icon={faSpinner} spin={true} /> : label}
            {...rest}
        />
    )
}

export default AsyncActionButton;