import {ReactNode} from "react";
import {IField} from "../../types";

export interface IBaseInput extends IField {
    type: string,
    icon?: ReactNode,
}

export interface ITextField extends IField {
}

export interface IEmailField extends IField {
}