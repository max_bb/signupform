import React from "react";
import { render, mount } from 'enzyme';

import BaseInput from "../BaseInput";

const testProps = {
    type: 'text',
    placeholder: 'mockPlaceholder',
    value: 'mockValue',
    onChange: () => void(0),
    icon: <div id="mockIcon" />
}

const commonWrapper = render(<BaseInput {...testProps} />);

describe('BaseInput component', () => {
    it('Should render placeholder', function () {
        const wrapper = render(<BaseInput {...testProps} value='' />);
        expect(wrapper.find('input').attr('placeholder')).toEqual(testProps.placeholder);
    });

    it('Should render value if it is specified', function () {
        expect(commonWrapper.find('input').attr('value')).toEqual(testProps.value);
    });

    it('Should render input of correct type', function () {
        expect(commonWrapper.find('input').attr('type')).toEqual(testProps.type);
    });

    it('Should render icon if it is specified', function () {
        expect(commonWrapper.find('#mockIcon')).toHaveLength(1);
    });

    it('Should fire onChange callback when changed', function () {
        const onChange = jest.fn();
        const wrapper = mount(<BaseInput {...testProps} onChange={onChange} />);
        const input = wrapper.find('input');
        input.simulate('change', { target: { value: 'newValue' } });
        expect(onChange).toHaveBeenCalledTimes(1);
        expect(onChange.mock.calls[0][0]).toEqual('newValue');
    });
})