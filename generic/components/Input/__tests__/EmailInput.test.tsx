import React from "react";
import { shallow } from 'enzyme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

import EmailInput from "../EmailInput";

const testProps = {
    placeholder: 'mockPlaceholder',
    value: 'mockValue',
    onChange: () => void(0),
}

const commonWrapper = shallow(<EmailInput {...testProps} />);

describe('EmailInput component', () => {
    it('Should pass correct props to BaseInput', function () {
        const baseInputWrapper = commonWrapper.find('BaseInput');
        expect(baseInputWrapper.prop('type')).toEqual('email');
        expect(baseInputWrapper.prop('placeholder')).toEqual(testProps.placeholder);
        expect(baseInputWrapper.prop('value')).toEqual(testProps.value);
        expect(baseInputWrapper.prop('onChange')).toEqual(testProps.onChange);
        expect(baseInputWrapper.prop('icon')).toEqual(<FontAwesomeIcon icon={faEnvelope} />);
    });
})