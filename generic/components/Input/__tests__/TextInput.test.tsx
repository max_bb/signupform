import React from "react";
import { shallow } from 'enzyme';

import TextInput from "../TextInput";

const testProps = {
    placeholder: 'mockPlaceholder',
    value: 'mockValue',
    onChange: () => void(0),
}

const commonWrapper = shallow(<TextInput {...testProps} />);

describe('TextInput component', () => {
    it('Should pass correct props to BaseInput', function () {
        const baseInputWrapper = commonWrapper.find('BaseInput');
        expect(baseInputWrapper.prop('type')).toEqual('text');
        expect(baseInputWrapper.prop('placeholder')).toEqual(testProps.placeholder);
        expect(baseInputWrapper.prop('value')).toEqual(testProps.value);
        expect(baseInputWrapper.prop('onChange')).toEqual(testProps.onChange);
    });
})