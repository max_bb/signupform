import React from "react";
import { shallow } from 'enzyme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock } from '@fortawesome/free-solid-svg-icons';

import PasswordInput from "../PasswordInput";

const testProps = {
    placeholder: 'mockPlaceholder',
    value: 'mockValue',
    onChange: () => void(0),
}

const commonWrapper = shallow(<PasswordInput {...testProps} />);

describe('PasswordInput component', () => {
    it('Should pass correct props to BaseInput', function () {
        const baseInputWrapper = commonWrapper.find('BaseInput');
        expect(baseInputWrapper.prop('type')).toEqual('password');
        expect(baseInputWrapper.prop('placeholder')).toEqual(testProps.placeholder);
        expect(baseInputWrapper.prop('value')).toEqual(testProps.value);
        expect(baseInputWrapper.prop('onChange')).toEqual(testProps.onChange);
        expect(baseInputWrapper.prop('icon')).toEqual(<FontAwesomeIcon icon={faLock} />);
    });
})