import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

import BaseInput from "./BaseInput";
import {IEmailField} from "./types";

const EmailInput: React.FC<IEmailField> = (
    {
        placeholder,
        value,
        onChange,
    }
) => {
    return (
        <BaseInput
            type="email"
            placeholder={placeholder}
            value={value}
            onChange={onChange}
            icon={<FontAwesomeIcon icon={faEnvelope} />}
        />
    )
}

export default EmailInput;