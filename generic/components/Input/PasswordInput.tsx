import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLock } from '@fortawesome/free-solid-svg-icons'

import BaseInput from "./BaseInput";
import {IEmailField} from "./types";

const PasswordInput: React.FC<IEmailField> = (
    {
        placeholder,
        value,
        onChange,
    }
) => {
    return (
        <BaseInput
            type="password"
            placeholder={placeholder}
            value={value}
            onChange={onChange}
            icon={<FontAwesomeIcon icon={faLock} />}
        />
    )
}

export default PasswordInput;