import React, {ChangeEvent, Ref, useRef} from "react";
import styled from "styled-components";
import {IBaseInput} from "./types";
import {disabledColor, focusIndication, textColor} from "../../styleConstants";
import useFocus from "../useFocus";

const StyledInputWrapper = styled.div<{isFocused: boolean}>`
    background-color: #F5F8FA;
    border-radius: 8px;
    padding: 17px;
    width: 100%;
    display: flex;
    align-items: center;
    height: 50px;
    cursor: text;
    
    ${props => props.isFocused && focusIndication}
`;

const StyledInput = styled.input`
    border: 0;
    outline: 0;
    font-size: 14px;
    color: ${textColor};
    width: 100%;
    background-color: inherit;
    
    ::placeholder {
      color: ${disabledColor};
    }
`;

const StyledIcon = styled.div`
    margin-right: 16px;
    color: ${disabledColor};
`;

const BaseInput: React.FC<IBaseInput> = (
    {
        type,
        placeholder,
        value,
        onChange,
        icon,
    }
) => {
    const inputRef = useRef<HTMLInputElement>();

    const {isFocused, handleBlur, handleFocus} = useFocus();

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        onChange(value);
    };

    const handleInputWrapperClick = () => {
        // Extending component's clickable area
        inputRef.current && inputRef.current.focus();
    };

    return (
        <StyledInputWrapper onClick={handleInputWrapperClick} isFocused={isFocused}>
            {icon && <StyledIcon>{icon}</StyledIcon>}
            <StyledInput
                ref={inputRef as Ref<HTMLInputElement>}
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
            />
        </StyledInputWrapper>
    )
}

export default BaseInput;