import React from "react";
import BaseInput from "./BaseInput";
import {ITextField} from "./types";

const TextInput: React.FC<ITextField> = (
    {
        placeholder,
        value,
        onChange,
    }
) => {
    return <BaseInput type="text" placeholder={placeholder} value={value} onChange={onChange}/>
}

export default TextInput;