import {useState} from "react";

const useFocus = () => {
    const [isFocused, setIsFocused] = useState<boolean>(false);

    const handleFocus = () => {
        setIsFocused(true);
    }

    const handleBlur = () => {
        setIsFocused(false);
    }

    return {
        handleFocus,
        handleBlur,
        isFocused
    }
};

export default useFocus;