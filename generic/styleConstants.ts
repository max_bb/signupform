export const textColor = '#222';
export const errorColor = '#E82828';
export const actionColor = '#0094FF';
export const disabledColor = '#A2A2A2';

export const focusIndication = `box-shadow: 0 0 5px 1px ${actionColor};`

export const stylesToHideNativeInput = `
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    cursor: inherit;
    margin: 0;
    padding: 0;
    opacity: 0;
    z-index: 1;
    position: absolute;
`