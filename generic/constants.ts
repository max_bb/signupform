import {IListValue} from "./types";

export const genderList: IListValue[] = [
    {
        identifier: 'MALE',
        value: 'Male',
    },
    {
        identifier: 'FEMALE',
        value: 'Female',
    },
];