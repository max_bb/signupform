export interface IField {
    placeholder?: string,
    value: string,
    onChange: (value: string) => void,
}

export interface IListValue {
    identifier: string,
    value: string,
}

export interface ISignUpFormValues {
    name: string,
    email: string,
    password: string,
    country: string,
    gender: string,
    acceptPolicies: boolean,
}