import Head from 'next/head'
import React from "react";
import styled from "styled-components";

import SignUp from "../components/SignUp";

const StyledContent = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    background-color: #102250;
`;

const SignUpPage = () => {
    return (
        <StyledContent>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href={'/favicon.ico'}/>
            </Head>

            <SignUp/>
        </StyledContent>
    );
};

export default SignUpPage;