import Head from 'next/head'
import styled from "styled-components";

import Link from "../components/Link";
import Layout from "../layouts/layout";

const StyledContent = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

export default function Index() {
    return (
        <Layout>
            <Head>
                <title>Index Page</title>
                <link rel="icon" href={'/favicon.ico'}/>
            </Head>

            <StyledContent>
                <Link href={'/SignUpPage'}>Go to SignUp page</Link>
            </StyledContent>

        </Layout>
    )
}
